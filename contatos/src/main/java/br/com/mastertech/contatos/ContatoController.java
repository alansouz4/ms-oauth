package br.com.mastertech.contatos;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/contato")
@RequiredArgsConstructor
public class ContatoController {

    private final ContatoRepository contatoRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato salvaContato(@RequestBody Contato contato) {
        return contatoRepository.save(contato);
    }

    @GetMapping
    public List<Contato> buscaContato() {
        return contatoRepository.findAll();
    }
}
